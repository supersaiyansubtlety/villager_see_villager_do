package net.sssubtlety.villager_see_villager_do;

import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

import net.sssubtlety.villager_see_villager_do.config.ConfigManager;

@Environment(EnvType.CLIENT)
public class ModMenuInit implements ModMenuApi {
    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        return ConfigManager.getScreenFactory()::apply;
    }
}

