package net.sssubtlety.villager_see_villager_do.mixin;

import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.ai.brain.sensor.NearestItemsSensor;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.VillagerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;

import net.sssubtlety.villager_see_villager_do.mixin_helper.VillagerEntityMixinAccessor;

import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin(NearestItemsSensor.class)
abstract class NearestItemsSensorMixin {
    // lambda in sensor(...) with MobEntity::canGather(...) call
    @WrapOperation(
        method = "method_24646",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/entity/mob/MobEntity;canGather(Lnet/minecraft/server/world/ServerWorld;" +
                "Lnet/minecraft/item/ItemStack;)Z"
        )
    )
    private static boolean passGatheringItemThrowerUuid(
        MobEntity mob, ServerWorld world, ItemStack stack,
        Operation<Boolean> original,
        MobEntity mobDuplicate, ServerWorld worldDuplicate, ItemEntity itemEntity
    ) {
        if (mob instanceof VillagerEntity villager) {
            final @Nullable Entity thrower = itemEntity.getOwner();
            ((VillagerEntityMixinAccessor) villager).villager_see_villager_do$setGatheringItemThrowerUuid(
                thrower == null ? null : thrower.getUuid()
            );
        }

        final boolean gatherable = original.call(mob, world, stack);

        if (mob instanceof VillagerEntity villager) {
            ((VillagerEntityMixinAccessor)villager).villager_see_villager_do$unsetGatheringItemThrowerUuid();
        }

        return gatherable;
    }
}
