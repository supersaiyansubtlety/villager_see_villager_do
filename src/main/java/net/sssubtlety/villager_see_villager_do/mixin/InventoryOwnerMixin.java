package net.sssubtlety.villager_see_villager_do.mixin;

import net.minecraft.entity.InventoryOwner;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.VillagerEntity;
import net.minecraft.server.world.ServerWorld;

import net.sssubtlety.villager_see_villager_do.mixin_helper.VillagerEntityMixinAccessor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(InventoryOwner.class)
interface InventoryOwnerMixin {
    @Inject(
        method = "pickUpItem",
        at = @At(
            value = "INVOKE", shift = At.Shift.AFTER,
            target = "Lnet/minecraft/entity/mob/MobEntity;sendPickup(Lnet/minecraft/entity/Entity;I)V"
        )
    )
    private static void checkAddedItems(
        ServerWorld world, MobEntity mob, InventoryOwner owner, ItemEntity itemEntity, CallbackInfo ci
    ) {
        if (mob instanceof VillagerEntity villagerEntity && !villagerEntity.isClient()) {
            ((VillagerEntityMixinAccessor)villagerEntity).villager_see_villager_do$onInventoryPickup(itemEntity);
        }
    }
}
