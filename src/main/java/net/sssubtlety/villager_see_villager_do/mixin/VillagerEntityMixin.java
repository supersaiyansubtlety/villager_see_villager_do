package net.sssubtlety.villager_see_villager_do.mixin;

import com.google.common.collect.Lists;
import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import com.llamalad7.mixinextras.sugar.Local;
import com.llamalad7.mixinextras.sugar.ref.LocalIntRef;
import com.llamalad7.mixinextras.sugar.ref.LocalRef;
import net.minecraft.component.DataComponentTypes;
import net.minecraft.component.type.PotionContentsComponent;
import net.minecraft.component.type.SuspiciousStewEffectsComponent;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.InteractionObserver;
import net.minecraft.entity.InventoryOwner;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.ai.brain.task.LookTargetUtil;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.VillagerEntity;
import net.minecraft.item.BoatItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtOps;
import net.minecraft.registry.Holder;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.Pair;
import net.minecraft.village.TradeOffer;
import net.minecraft.village.TradeOfferList;
import net.minecraft.village.TradeOffers;
import net.minecraft.village.TradeableItem;
import net.minecraft.village.VillagerData;
import net.minecraft.village.VillagerDataContainer;
import net.minecraft.village.VillagerProfession;

import net.sssubtlety.villager_see_villager_do.TradeTargetManager;
import net.sssubtlety.villager_see_villager_do.config.ConfigManager;
import net.sssubtlety.villager_see_villager_do.mixin_helper.VillagerEntityMixinAccessor;
import net.sssubtlety.villager_see_villager_do.util.OfferResponse;
import net.sssubtlety.villager_see_villager_do.util.OfferResponses;
import net.sssubtlety.villager_see_villager_do.util.Util;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;

import static net.sssubtlety.villager_see_villager_do.VillagerSeeVillagerDo.LOGGER;
import static net.sssubtlety.villager_see_villager_do.mixin_helper.VillagerEntityMixinConstants.*;

@Mixin(VillagerEntity.class)
abstract class VillagerEntityMixin extends MerchantEntityMixin implements
        InteractionObserver, VillagerDataContainer, VillagerEntityMixinAccessor {
    @Shadow public abstract int getExperience();
    @Shadow public abstract VillagerData getVillagerData();
    @Shadow protected abstract void fillRecipes();
    @Shadow protected abstract void sayNo();
    @Shadow protected abstract void resetCustomer();

    @Unique
    @SuppressWarnings("NotNullFieldNotInitialized")
    @NotNull
    private PersistentData persistentData;
    @Unique
    @SuppressWarnings("NotNullFieldNotInitialized")
    @NotNull
    private ThreadLocal<UUID> gatheringItemThrowerUuid;

    @Override
    public void villager_see_villager_do$onInventoryPickup(ItemEntity itemEntity) {
        final var profession = this.getVillagerData().getProfession();
        if (profession == VillagerProfession.NONE) {
            return;
        }

        final var entityStack = itemEntity.getStack();
        final var itemThrower = itemEntity.getOwner();
        if (itemThrower != null) {
            this.persistentData.recentInteractorUuid = itemThrower.getUuid();
        }

        if (ConfigManager.canPayEmeraldToDropItems() && entityStack.getItem() == Items.EMERALD) {
            final var inventory = this.getInventory();
            if (!inventory.isEmpty()) {
                // consume emerald, clear inventory
                final int size = inventory.size();
                for (int i = 0; i < size; i++) {
                    final ItemStack right = inventory.getStack(i);
                    if (ItemStack.areEqual(entityStack, right)) {
                        inventory.removeStack(i, 1);
                        this.persistentData.targetManager.clearAll();
                        // reset trades so trades can't be set without the villager taking an item
                        // items that set a trade are meant to be kept by the villager
                        this.resetTrades();
                        this.throwStacks(inventory.clearToList());
                        entityStack.decrement(1);

                        break;
                    }
                }
            }
        } else {
            if (this.isTradable(entityStack.getItem())) {
                this.persistentData.targetManager.add(entityStack);
                this.resetTrades();
            }
        }
    }

    @Override
    public void villager_see_villager_do$setGatheringItemThrowerUuid(@Nullable UUID uuid) {
        if (uuid == null) {
            this.gatheringItemThrowerUuid.remove();
        } else {
            this.gatheringItemThrowerUuid.set(uuid);
        }
    }

    @Override
    public void villager_see_villager_do$unsetGatheringItemThrowerUuid() {
        this.gatheringItemThrowerUuid.remove();
    }

    @Override
    protected boolean passGatheringItemThrowerUuid(
        MobEntity mob, ServerWorld world, ItemStack stack,
        Operation<Boolean> original,
        @NotNull ItemEntity itemEntity
    ) {
        final @Nullable Entity thrower = itemEntity.getOwner();
        this.villager_see_villager_do$setGatheringItemThrowerUuid(thrower == null ? null : thrower.getUuid());

        final boolean gatherable = super.passGatheringItemThrowerUuid(mob, world, stack, original, itemEntity);

        this.villager_see_villager_do$unsetGatheringItemThrowerUuid();

        return gatherable;
    }

    // overrides stub handler method in MerchantEntityMixin
    @Override
    protected void modifyPool(
        TradeOfferList recipeList, TradeOffers.Factory[] pool, int count, CallbackInfo ci,
        @Local(ordinal = 0, argsOnly = true) LocalRef<TradeOffers.Factory[]> poolRef,
        @Local(ordinal = 0, argsOnly = true) LocalIntRef countRef
    ) {
        if (!this.getWorld().isClient()) {
            final var modifiedVariables = this.tryFillTargetTradesFirst(recipeList, pool, count);
            poolRef.set(modifiedVariables.getLeft());
            countRef.set(modifiedVariables.getRight());
        }
    }

    @Inject(method = "setCurrentCustomer", at = @At("TAIL"))
    private void setRecentCustomer(CallbackInfo ci) {
        final var currentCustomer = this.getCurrentCustomer();
        if (currentCustomer != null) {
            this.persistentData.recentInteractorUuid = currentCustomer.getUuid();
        }
    }

    @Inject(
        method = "<init>(Lnet/minecraft/entity/EntityType;Lnet/minecraft/world/World;" +
            "Lnet/minecraft/village/VillagerType;)V",
        at = @At("TAIL")
    )
    private void initFields(CallbackInfo info) {
        this.persistentData = new PersistentData(new TradeTargetManager(), 0, null);
        this.gatheringItemThrowerUuid = new ThreadLocal<>();
    }

    // "pass" the uuid of the owner of the thrown item to allowGatheringTradables
    @WrapOperation(
        method = "loot",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/entity/InventoryOwner;pickUpItem(Lnet/minecraft/server/world/ServerWorld;" +
                "Lnet/minecraft/entity/mob/MobEntity;Lnet/minecraft/entity/InventoryOwner;" +
                "Lnet/minecraft/entity/ItemEntity;)V"
        )
    )
    private void passGatheringItemThrowerUuid(
        ServerWorld world, MobEntity thisEntity, InventoryOwner thisOwner, ItemEntity itemEntity,
        Operation<Void> original
    ) {
        @Nullable final Entity thrower = itemEntity.getOwner();
        this.villager_see_villager_do$setGatheringItemThrowerUuid(thrower == null ? null : thrower.getUuid());

        original.call(world, thisEntity, thisOwner, itemEntity);

        this.villager_see_villager_do$unsetGatheringItemThrowerUuid();
    }

    @ModifyExpressionValue(
        method = "canGather",
        at = @At(
            value = "INVOKE", remap = false,
            target = "Lcom/google/common/collect/ImmutableSet;contains(Ljava/lang/Object;)Z"
        )
    )
    private boolean allowGatheringTradables(final boolean original, ServerWorld world, ItemStack stack) {
        final Item item;
        if (
            this.getExperience() > 0 ||
            this.isSleeping() ||
            ConfigManager.isPickupDenied(item = stack.getItem())
        ) {
            return original;
        }

        final var profession = this.getVillagerData().getProfession();
        if (profession == VillagerProfession.NONE) {
            return original;
        }

        if (this.getUuid().equals(this.gatheringItemThrowerUuid.get())) {
            return false;
        }

        if (ConfigManager.canPayEmeraldToDropItems() && item == Items.EMERALD) {
            final var inventory = this.getInventory();
            if (!inventory.isEmpty()) {
                // gather emerald as payment for returning items
                if (!inventory.canInsert(stack))
                    // if inventory is full, return last stack to make space
                {
                    this.throwStacks(Lists.newArrayList(inventory.removeStack(inventory.size() - 1)));
                }

                return true;
            }
        }

        return original || this.isTradable(item);
    }

    @Inject(method = "onDeath", at = @At("HEAD"))
    private void tryDropItems(DamageSource source, CallbackInfo ci) {
        if (ConfigManager.shouldVillagerDropItemsOnDeath()) {
            ItemScatterer.spawn(this.getWorld(), this, this.getInventory());
        }
    }

    @Inject(method = "afterUsing", at = @At("HEAD"))
    private void commitPending(CallbackInfo ci) {
        this.commitAndThrowPending(true);
    }

    @Inject(method = "writeCustomDataToNbt", at = @At("TAIL"))
    private void writePersistentDataToNbt(NbtCompound nbt, CallbackInfo ci) {
        PersistentData.CODEC.encodeStart(
            this.getRegistryManager().createSerializationContext(NbtOps.INSTANCE),
                this.persistentData
        )
            .ifSuccess(result -> nbt.put(DATA_KEY, result))
            .ifError(partial ->
                LOGGER.error("Error serializing VillagerEntityMixin data: {}", partial)
            );
    }

    @Inject(method = "readCustomDataFromNbt", at = @At("TAIL"))
    private void readPersistentDataFromNbt(NbtCompound nbt, CallbackInfo ci) {
        final var dataResult = PersistentData.CODEC.parse(
            this.getRegistryManager().createSerializationContext(NbtOps.INSTANCE),
            nbt.get(DATA_KEY)
        );

        //noinspection deprecation
        this.persistentData = dataResult.result().orElseGet(
            () -> DeprecatedDeserialization.tryParseNbt(nbt, this.getRegistryManager())
        );
    }

    @Inject(method = "onDeath", at = @At("HEAD"))
    private void tryDropItemsOnDeath(DamageSource source, CallbackInfo ci) {
        if (ConfigManager.shouldVillagerDropItemsOnDeath()) {
            ItemScatterer.spawn(
                this.getWorld(), this, this.getInventory()
            );
        }
    }

    @Inject(method = "mobTick", at = @At("TAIL"))
    private void tryThrowPendingRejection(CallbackInfo ci) {
        if (this.persistentData.rejectionWaitTicks > 0) {
            this.persistentData.rejectionWaitTicks--;
            if (this.persistentData.rejectionWaitTicks == 0) {
                this.commitAndThrowRejected();
            }
        }
    }

    @Unique
    private boolean isTradable(Item item) {
        final var profession = this.getVillagerData().getProfession();
        return PROFESSION_TRADABLE_ITEMS.get(profession).contains(item) || (
            profession == VillagerProfession.FISHERMAN &&
            VILLAGER_TYPE_BOATS.get(this.getVillagerData().getType()) == item
        );
    }

    // count is number of trades to add
    // pool is list of possible trade factories
    @Unique
    private Pair<TradeOffers.Factory[], Integer> tryFillTargetTradesFirst(
        TradeOfferList tradeList, TradeOffers.Factory[] pool, int count
    ) {
        if (!this.isAlive()) {
            return new Pair<>(pool, count);
        }

        final boolean experienced = this.getExperience() > 0;

        if (!experienced) {
            this.persistentData.targetManager.restorePendingSelection();
        }

        if (this.persistentData.targetManager.isTargetsEmpty()) {
            return new Pair<>(pool, count);
        }

        final List<OfferAndIndex> acceptedOffers = new LinkedList<>();
        final var factories = Lists.newArrayList(pool);
        final var factoryItr = factories.iterator();
        while (factoryItr.hasNext() && count > 0 && !this.persistentData.targetManager.isTargetsEmpty()) {
            final var factory = factoryItr.next();
            final var offer = factory.create(this, this.random);

            if (offer != null) {
                final var acceptedOffer = this.tryAdjustOffer(factory, offer);
                if (acceptedOffer.isPresent()) {
                    acceptedOffers.add(acceptedOffer.get());
                    count--;
                    factoryItr.remove();
                }
            }
        }

        this.commitAndThrowPending(experienced);
        if (
            experienced && this.getVillagerData().getLevel() >= 5
                && !this.persistentData.targetManager.isTargetsEmpty()
        ) {
            if (this.removeAndThrowMatchingStacksFromInventory(
                this.persistentData.targetManager::getIterator,
                TradeTargetManager.StackAndIndex::stack
            )) {
                this.sayNo();
            }

            this.persistentData.targetManager.clearAll();
        }

        tradeList.addAll(acceptedOffers.stream()
            .sorted(Comparator.comparingInt(OfferAndIndex::index))
            .map(OfferAndIndex::offer)
            .toList()
        );

        return new Pair<>(factories.toArray(new TradeOffers.Factory[0]), count);
    }

    @Unique
    private TradeOffer createAdjustedOffer(TradeOffer offer, ItemStack target, boolean isSellItem) {
        final TradeableItem firstBuyItem;
        final ItemStack sellStack;

        Util.stripNbt(target);

        if (isSellItem) {
            final var oldFirstBuyItem = offer.getFirstBuyItem();
            final var originalFirstBuyItemCopy = offer.getOriginalFirstBuyItem().copy();
            firstBuyItem = new TradeableItem(
                oldFirstBuyItem.item(), originalFirstBuyItemCopy.getCount(),
                oldFirstBuyItem.components(), originalFirstBuyItemCopy
            );

            sellStack = target;
            sellStack.setCount(offer.getSellItem().getCount());
        } else {
            final int count = offer.getOriginalFirstBuyItem().getCount();
            target.setCount(count);

            firstBuyItem = new TradeableItem(
                target.getHolder(), count,
                Util.createComponentPredicate(target), target
            );

            sellStack = offer.copySellItem();
        }

        return new TradeOffer(
            firstBuyItem, offer.getSecondBuyItem(), sellStack,
            offer.getUses(), offer.getMaxUses(), offer.getMerchantExperience(),
            offer.getPriceMultiplier(), offer.getDemandBonus()
        );
    }

    @Unique
    private Optional<OfferAndIndex> tryAdjustOffer(@NotNull TradeOffers.Factory factory, @NotNull TradeOffer offer) {
        final var firstBuyStack = offer.getOriginalFirstBuyItem();
        // left side, no non-default components
        if (!Util.hasSubstantialComponents(firstBuyStack)) {
            final Item firstBuyItem = firstBuyStack.getItem();
            if (firstBuyItem != Items.EMERALD) {
                final Optional<OfferAndIndex> optOfferAndIndex = this.findTargetAndDoRejections(
                    firstBuyItem,
                    stack -> this.considerComponentlessOffer(offer, stack, false)
                );
                if (optOfferAndIndex.isPresent()) {
                    return optOfferAndIndex;
                }
            }
        }

        final var sellStack = offer.getSellItem();
        final var sellItem = sellStack.getItem();
        if (sellItem == Items.EMERALD) {
            return Optional.empty();
        } else {
            // right side, can be tagged
            if (EnchantmentHelper.hasEnchantments(sellStack)) {
                // has enchantments
                return this.findTargetAndDoRejections(sellItem,
                    stack -> this.adjustOrRejectEnchantedOffer(factory, offer, stack)
                );
            } else if (sellItem == Items.TIPPED_ARROW) {
                return this.findTargetAndDoRejections(sellItem,
                    stack -> this.adjustOrRejectTippedArrowOffer(offer, stack)
                );
            } else if (sellItem == Items.SUSPICIOUS_STEW) {
                return this.findTargetAndDoRejections(sellItem,
                    stack -> this.adjustOrRejectSuspiciousStewOffer(offer, stack)
                );
            } else {
                return this.findTargetAndDoRejections(sellItem,
                    stack -> this.considerComponentlessOffer(offer, stack, true)
                );
            }
        }
    }

    @Unique
    private Optional<OfferAndIndex> findTargetAndDoRejections(
        Item targetItem, Function<ItemStack, OfferResponse> adjuster
    ) {
        OfferAndIndex selected = null;
        final TradeTargetManager.TradeTargetsIterator targetItr =
            this.persistentData.targetManager.getIterator(targetItem);
        while (targetItr.hasNext()) {
            final TradeTargetManager.StackAndIndex target = targetItr.next();

            switch (adjuster.apply(target.stack())) {
                case OfferResponse.Accept accept -> {
                    if (selected == null) {
                        targetItr.select();
                        selected = new OfferAndIndex(accept.offerFactory.get(), target.index());
                    }
                }
                case OfferResponse.Reject ignored -> targetItr.reject();
                case OfferResponse.NonNegative ignored ->  {
                    // skip PASS
                }
            }
        }

        return Optional.ofNullable(selected);
    }

    @Unique
    private OfferResponse considerComponentlessOffer(TradeOffer offer, ItemStack target, boolean isSellItem) {
        final Item targetItem = target.getItem();

        if (Util.hasSubstantialComponents(target)) {
            return isSellItem && LATER_ENCHANTABLE.contains(targetItem) ?
                OfferResponses.PASS :
                OfferResponses.REJECT;
        } else {
            return targetItem instanceof BoatItem boat &&
                boat != VILLAGER_TYPE_BOATS.get(this.getVillagerData().getType()) ?
                    OfferResponses.REJECT :
                    OfferResponses.accept(() -> this.createAdjustedOffer(offer, target, isSellItem));
        }
    }

    @Unique
    private OfferResponse adjustOrRejectEnchantedOffer(
        TradeOffers.Factory factory, TradeOffer offer, ItemStack target
    ) {
        if (EnchantmentHelper.hasEnchantments(target)) {
            final Optional<Integer> cost = this.getWorld()
                .getRegistryManager()
                .getLookup(RegistryKeys.ENCHANTMENT)
                .flatMap(enchantments ->
                    Util.tryCalculateEnchantmentsCost(target, factory, enchantments)
                );

            if (cost.isPresent()) {
                Util.setEmeralds(offer, cost.orElseThrow());

                return OfferResponses.accept(() -> this.createAdjustedOffer(offer, target, true));
            }
        }

        return OfferResponses.REJECT;
    }

    @Unique
    private OfferResponse adjustOrRejectTippedArrowOffer(TradeOffer offer, ItemStack target) {
        final boolean potionAllowed = target
            .getOrDefault(DataComponentTypes.POTION_CONTENTS, PotionContentsComponent.DEFAULT)
            .potion()
            .map(Holder::getValue)
            .filter(potion -> ConfigManager.isAllowed(potion, this.getWorld().getBrewingRecipeRegistry()))
            .isPresent();

        return potionAllowed ?
            OfferResponses.accept(() -> this.createAdjustedOffer(offer, target, true)) :
            OfferResponses.REJECT;
    }

    @Unique
    private OfferResponse adjustOrRejectSuspiciousStewOffer(TradeOffer offer, ItemStack target) {
         final var effectsAndDurations = target.getOrDefault(
            DataComponentTypes.SUSPICIOUS_STEW_EFFECTS, SuspiciousStewEffectsComponent.DEFAULT
        ).effects();

        if (ConfigManager.shouldLimitEnchantmentsEtc() && effectsAndDurations.size() != 1) {
            return OfferResponses.REJECT;
        }

        for (final SuspiciousStewEffectsComponent.Entry effectAndDuration : effectsAndDurations) {
            if (!ConfigManager.isAllowed(
                effectAndDuration.effect().getValue(),
                effectAndDuration.duration()
            )) {
                return OfferResponses.REJECT;
            }
        }

        return OfferResponses.accept(() -> this.createAdjustedOffer(offer, target, true));
    }

    @Unique
    private <S> boolean removeAndThrowMatchingStacksFromInventory(
        Function<Item, Iterator<S>> stackConvertibleItrGetter, Function<S, ItemStack> stackConverter
    ) {
        final var inventory = this.getInventory();
        final int size = inventory.size();
        final List<ItemStack> removedFromInventory = new LinkedList<>();
        for (int slot = 0; slot < size; slot++) {
            final var curStack = inventory.getStack(slot);
            final var stackConvertibleItr = stackConvertibleItrGetter.apply(curStack.getItem());
            while (stackConvertibleItr.hasNext()) {
                final S stackConvertible = stackConvertibleItr.next();
                final var convertedStack = stackConverter.apply(stackConvertible);
                if (ItemStack.itemsAndComponentsMatch(curStack, convertedStack)) {
                    stackConvertibleItr.remove();
                    final int convertedStackCount = convertedStack.getCount();
                    if (convertedStackCount > 0) {
                        removedFromInventory.add(inventory.removeStack(slot, convertedStackCount));
                    }
                }
            }
        }

        if (!removedFromInventory.isEmpty()) {
            this.throwStacks(removedFromInventory);

            return true;
        } else {
            return false;
        }
    }

    @Unique
    private void commitAndThrowPending(boolean experienced) {
        if (!experienced || !this.tryCommitAndThrowSelected()) {
            this.commitAndThrowRejected();
        }
    }

    @Unique
    private boolean tryCommitAndThrowSelected() {
        final var selections = this.persistentData.targetManager.commitSelection();
        if (
            ConfigManager.shouldReturnSelectedItems() && !selections.isEmpty() &&
            // throw back any selected stacks
                this.removeAndThrowMatchingStacksFromInventory(
                item -> selections.iterator(),
                stack -> stack
            )
        ) {
            this.playCelebrateSound();
            this.persistentData.rejectionWaitTicks = MAX_REJECTION_WAIT_TICKS;

            return true;
        }

        return false;
    }

    @Unique
    private void commitAndThrowRejected() {
        final var rejections = this.persistentData.targetManager.commitRejection();
        if (!rejections.isEmpty()) {
            if (this.removeAndThrowMatchingStacksFromInventory(
                item -> rejections.iterator(),
                stack -> stack
            )) {
                this.sayNo();
            }

            if (!rejections.isEmpty()) {
                LOGGER.error(
                    "Found rejected stacks that aren't in inventory: {}", Util.toString(rejections)
                );
            }
        }
    }

    @Unique
    private void throwStacks(Iterable<ItemStack> stacks) {
        if (!(this.getWorld() instanceof ServerWorld serverWorld)) {
            return;
        }

        final @Nullable Entity recentInteractor;
        final @NotNull Entity targetEntity =
            this.persistentData.recentInteractorUuid != null &&
            (recentInteractor = serverWorld.getEntity(this.persistentData.recentInteractorUuid)) != null &&
            this.distanceTo(recentInteractor) <= MAX_THROW_TARGET_DIST ?
                recentInteractor : this;

        for (final var invStack : stacks) {
            LookTargetUtil.throwStack(this, invStack, targetEntity.getPos());
        }
    }

    @Unique
    private void resetTrades() {
        this.resetCustomer();
        this.offers = new TradeOfferList();
        this.fillRecipes();
    }
}
