package net.sssubtlety.villager_see_villager_do.mixin;

import com.llamalad7.mixinextras.sugar.Local;
import com.llamalad7.mixinextras.sugar.ref.LocalIntRef;
import com.llamalad7.mixinextras.sugar.ref.LocalRef;
import net.minecraft.entity.passive.MerchantEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.village.TradeOfferList;
import net.minecraft.village.TradeOffers;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(MerchantEntity.class)
abstract class MerchantEntityMixin extends MobEntityMixin {
    @Shadow
    @Nullable
    public abstract PlayerEntity getCurrentCustomer();

    @Shadow
    public abstract SimpleInventory getInventory();

    @Shadow
    public abstract void playCelebrateSound();

    @Shadow
    @Nullable
    protected TradeOfferList offers;

    // stub handler to be overriden in VillagerEntityMixin
    @Inject(method = "fillRecipesFromPool", at = @At("HEAD"))
    protected void modifyPool(
        TradeOfferList recipeList, TradeOffers.Factory[] pool, int count, CallbackInfo ci,
        @Local(ordinal = 0, argsOnly = true) LocalRef<TradeOffers.Factory[]> poolRef,
        @Local(ordinal = 0, argsOnly = true) LocalIntRef countRef
    ) { }
}
