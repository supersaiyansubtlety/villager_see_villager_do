package net.sssubtlety.villager_see_villager_do.mixin;

import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import com.llamalad7.mixinextras.sugar.Local;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;

import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin(MobEntity.class)
abstract class MobEntityMixin extends LivingEntity {
    protected MobEntityMixin() {
        //noinspection DataFlowIssue
        super(null, null);
        throw new IllegalStateException("MobEntityMixin's dummy constructor called!");
    }

    // stub handler to be overriden in VillagerEntityMixin
    @WrapOperation(
        method = "tickMovement",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/entity/mob/MobEntity;canGather(Lnet/minecraft/server/world/ServerWorld;" +
                "Lnet/minecraft/item/ItemStack;)Z"
        )
    )
    protected boolean passGatheringItemThrowerUuid(
        MobEntity mob, ServerWorld world, ItemStack stack,
        Operation<Boolean> original,
        @Local @NotNull ItemEntity itemEntity
    ) {
        return original.call(mob, world, stack);
    }
}
