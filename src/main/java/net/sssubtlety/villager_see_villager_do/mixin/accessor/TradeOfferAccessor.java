package net.sssubtlety.villager_see_villager_do.mixin.accessor;

import net.minecraft.village.TradeOffer;
import net.minecraft.village.TradeableItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;

import java.util.Optional;

@Mixin(TradeOffer.class)
public interface TradeOfferAccessor {
    @Mutable
    @Accessor("secondBuyItem")
    void villager_see_villager_do$setSecondBuyItem(
        @SuppressWarnings("OptionalUsedAsFieldOrParameterType") Optional<TradeableItem> secondBuyItem
    );
}
