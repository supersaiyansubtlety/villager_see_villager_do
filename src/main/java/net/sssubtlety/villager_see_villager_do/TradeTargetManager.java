package net.sssubtlety.villager_see_villager_do;

import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.TreeMultiset;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static net.sssubtlety.villager_see_villager_do.VillagerSeeVillagerDo.NAMESPACE;

public final class TradeTargetManager {
    public static final Codec<TradeTargetManager> CODEC = RecordCodecBuilder.create(
        instance -> instance.group(
            Codec.list(StackAndIndex.CODEC)
                .fieldOf(Keys.TRADE_TARGETS)
                .forGetter(manager -> manager.tradeTargets.values().stream().toList()),
            Codec.list(StackAndIndex.CODEC)
                .fieldOf(Keys.PENDING_SELECTION_AND_INDICES)
                .forGetter(manager -> manager.pendingSelection),
            Codec.list(ItemStack.CODEC)
                .fieldOf(Keys.PENDING_REJECTION)
                .forGetter(manager -> manager.pendingRejection)
        ).apply(instance, TradeTargetManager::new)
    );

    private final Multimap<Item, StackAndIndex> tradeTargets;
    private final List<StackAndIndex> pendingSelection;
    private final List<ItemStack> pendingRejection;

    public TradeTargetManager() {
        this.tradeTargets = Multimaps.newMultimap(
            new HashMap<>(),
            () -> TreeMultiset.create(Comparator.comparingInt(StackAndIndex::index))
        );
        this.pendingSelection = new ArrayList<>();
        this.pendingRejection = new ArrayList<>();
    }

    public TradeTargetManager(
        List<StackAndIndex> tradeTargets,
        List<StackAndIndex> stacksPendingSelection,
        List<ItemStack> stacksPendingRejection
    ) {
        this();

        for (final StackAndIndex stackAndIndex : tradeTargets) {
            this.add(stackAndIndex);
        }

        this.pendingSelection.addAll(stacksPendingSelection);
        this.pendingRejection.addAll(stacksPendingRejection);
    }

    public void clearAll() {
        this.clearTargets();
        this.pendingSelection.clear();
        this.pendingRejection.clear();
    }

    public void clearTargets() {
        this.tradeTargets.clear();
    }

    public @NotNull List<ItemStack> commitSelection() {
        final List<ItemStack> selected = this.pendingSelection.stream()
            .map(StackAndIndex::stack)
            .collect(Collectors.toCollection(ArrayList::new));

        this.pendingSelection.clear();

        return selected;
    }

    public @NotNull List<ItemStack> commitRejection() {
        final List<ItemStack> rejected = new ArrayList<>(this.pendingRejection);
        this.pendingRejection.clear();

        return rejected;
    }

    public boolean isTargetsEmpty() {
        return this.tradeTargets.isEmpty();
    }

    public void add(ItemStack target) {
        this.add(target, this.tradeTargets.size() + this.pendingSelection.size());
    }

    private void add(ItemStack target, int index) {
        this.add(new StackAndIndex(target, index));
    }

    private void add(StackAndIndex stackAndIndex) {
        this.tradeTargets.put(stackAndIndex.stack.getItem(), stackAndIndex);
    }

    public void restorePendingSelection() {
        for (final StackAndIndex stackAndIndex : this.pendingSelection) {
            final Collection<StackAndIndex> itemTargets = this.tradeTargets.get(stackAndIndex.stack.getItem());
            itemTargets.add(new StackAndIndex(stackAndIndex.stack, stackAndIndex.index));
        }

        this.pendingSelection.clear();
    }

    public TradeTargetsIterator getIterator(Item item) {
        return new TradeTargetsIterator(item);
    }

    public final class TradeTargetsIterator implements Iterator<StackAndIndex> {
        private final Iterator<StackAndIndex> targetsItr;
        @Nullable
        private StackAndIndex currentValue;

        private TradeTargetsIterator(Item item) {
            this.targetsItr = TradeTargetManager.this.tradeTargets.get(item).iterator();
            this.currentValue = null;
        }

        @Override
        public boolean hasNext() {
            return this.targetsItr.hasNext();
        }

        @Override
        public StackAndIndex next() {
            this.currentValue = this.targetsItr.next();
            return this.currentValue;
        }

        @Override
        public void remove() {
            this.targetsItr.remove();
        }

        public void select() {
            this.remove();
            // currentStack is only null if next() hasn't been called, in which case the above throws an exception
            // noinspection ConstantConditions
            TradeTargetManager.this.pendingSelection
                .add(new StackAndIndex(this.currentValue.stack(), this.currentValue.index));
        }

        public void reject() {
            this.remove();
            // currentStack is only null if next() hasn't been called, in which case the above throws an exception
            // noinspection ConstantConditions
            TradeTargetManager.this.pendingRejection.add(this.currentValue.stack());
        }
    }

    public record StackAndIndex(ItemStack stack, int index) {
        public static final Codec<StackAndIndex> CODEC = RecordCodecBuilder.create(instance -> instance.group(
            ItemStack.CODEC.fieldOf("stack").forGetter(stackAndIndex -> stackAndIndex.stack),
            Codec.INT.fieldOf("index").forGetter(stackAndIndex -> stackAndIndex.index)
        ).apply(instance, StackAndIndex::new));

        @Override
        public ItemStack stack() {
            return this.stack.copy();
        }
    }

    private interface Keys {
        String TRADE_TARGETS = NAMESPACE + ":trade_targets";
        String PENDING_SELECTION_AND_INDICES = NAMESPACE + ":pending_selection_and_indices";
        String PENDING_REJECTION = NAMESPACE + ":pending_rejection";
    }
}
