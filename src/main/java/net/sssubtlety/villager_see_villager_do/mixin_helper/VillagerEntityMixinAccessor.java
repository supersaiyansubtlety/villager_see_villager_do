package net.sssubtlety.villager_see_villager_do.mixin_helper;

import net.minecraft.entity.ItemEntity;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

public interface VillagerEntityMixinAccessor {
    void villager_see_villager_do$onInventoryPickup(ItemEntity itemEntity);
    void villager_see_villager_do$setGatheringItemThrowerUuid(@Nullable UUID uuid);
    void villager_see_villager_do$unsetGatheringItemThrowerUuid();
}
