package net.sssubtlety.villager_see_villager_do.mixin_helper;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtList;
import net.minecraft.registry.HolderLookup;
import net.minecraft.village.TradeOffer;
import net.minecraft.village.VillagerProfession;
import net.minecraft.village.VillagerType;
import net.sssubtlety.villager_see_villager_do.TradeTargetManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static net.sssubtlety.villager_see_villager_do.TradeTargetManager.StackAndIndex;
import static net.sssubtlety.villager_see_villager_do.VillagerSeeVillagerDo.LOGGER;
import static net.sssubtlety.villager_see_villager_do.VillagerSeeVillagerDo.NAMESPACE;

public final class VillagerEntityMixinConstants {
    public static final String DATA_KEY = NAMESPACE + ":data";

    public static final ImmutableSetMultimap<VillagerProfession, Item> PROFESSION_TRADABLE_ITEMS;
    public static final ImmutableMap<VillagerType, Item> VILLAGER_TYPE_BOATS;
    public static final ImmutableSet<Item> LATER_ENCHANTABLE;

    public static final int MAX_REJECTION_WAIT_TICKS = 20;
    public static final int MAX_THROW_TARGET_DIST = 8;

    static {
        final var professionTradableItemsBuilder = ImmutableSetMultimap.<VillagerProfession, Item>builder();

        professionTradableItemsBuilder.putAll(VillagerProfession.FARMER,
            Items.WHEAT, Items.POTATO, Items.CARROT, Items.BEETROOT, Items.BREAD,
            Items.PUMPKIN, Items.PUMPKIN_PIE, Items.APPLE,
            Items.COOKIE, Items.MELON,
            Items.CAKE, Items.SUSPICIOUS_STEW,
            Items.GOLDEN_CARROT, Items.GLISTERING_MELON_SLICE
        );
        professionTradableItemsBuilder.putAll(VillagerProfession.FISHERMAN,
            Items.STRING, Items.COAL, Items.COD, Items.COOKED_COD, Items.COD_BUCKET,
            Items.COD, Items.SALMON, Items.COOKED_SALMON, Items.CAMPFIRE,
            Items.SALMON, Items.FISHING_ROD,
            Items.TROPICAL_FISH,
            Items.PUFFERFISH // boats handled by VILLAGER_TYPE_BOATS
        );
        professionTradableItemsBuilder.putAll(VillagerProfession.SHEPHERD,
            Items.WHITE_WOOL, Items.BROWN_WOOL, Items.BLACK_WOOL, Items.GRAY_WOOL, Items.SHEARS,
            Items.WHITE_DYE, Items.GRAY_DYE, Items.BLACK_DYE, Items.LIGHT_BLUE_DYE, Items.LIME_DYE,
            Items.WHITE_WOOL, Items.ORANGE_WOOL, Items.MAGENTA_WOOL, Items.LIGHT_BLUE_WOOL,
                Items.YELLOW_WOOL, Items.LIME_WOOL, Items.PINK_WOOL, Items.GRAY_WOOL,
                Items.LIGHT_GRAY_WOOL, Items.CYAN_WOOL, Items.PURPLE_WOOL, Items.BLUE_WOOL,
                Items.BROWN_WOOL, Items.GREEN_WOOL, Items.RED_WOOL, Items.BLACK_WOOL,
                Items.WHITE_CARPET, Items.ORANGE_CARPET, Items.MAGENTA_CARPET, Items.LIGHT_BLUE_CARPET,
                Items.YELLOW_CARPET, Items.LIME_CARPET, Items.PINK_CARPET, Items.GRAY_CARPET,
                Items.LIGHT_GRAY_CARPET, Items.CYAN_CARPET, Items.PURPLE_CARPET, Items.BLUE_CARPET,
                Items.BROWN_CARPET, Items.GREEN_CARPET, Items.RED_CARPET, Items.BLACK_CARPET,
            Items.YELLOW_DYE, Items.LIGHT_GRAY_DYE, Items.ORANGE_DYE, Items.RED_DYE, Items.PINK_DYE,
                Items.WHITE_BED, Items.YELLOW_BED, Items.RED_BED, Items.BLACK_BED,
                Items.BLUE_BED, Items.BROWN_BED, Items.CYAN_BED, Items.GRAY_BED,
                Items.GREEN_BED, Items.LIGHT_BLUE_BED, Items.LIGHT_GRAY_BED, Items.LIME_BED,
                Items.MAGENTA_BED, Items.ORANGE_BED, Items.PINK_BED, Items.PURPLE_BED,
            Items.BROWN_DYE, Items.PURPLE_DYE, Items.BLUE_DYE, Items.GREEN_DYE, Items.MAGENTA_DYE, Items.CYAN_DYE,
                Items.WHITE_BANNER, Items.BLUE_BANNER, Items.LIGHT_BLUE_BANNER, Items.RED_BANNER,
                Items.PINK_BANNER, Items.GREEN_BANNER, Items.LIME_BANNER, Items.GRAY_BANNER,
                Items.BLACK_BANNER, Items.PURPLE_BANNER, Items.MAGENTA_BANNER, Items.CYAN_BANNER,
                Items.BROWN_BANNER, Items.YELLOW_BANNER, Items.ORANGE_BANNER, Items.LIGHT_GRAY_BANNER,
            Items.PAINTING
        );
        professionTradableItemsBuilder.putAll(VillagerProfession.FLETCHER,
            Items.STICK, Items.ARROW, Items.GRAVEL, // Items.FLINT,
            Items.FLINT, Items.BOW,
            Items.STRING, Items.CROSSBOW,
            Items.FEATHER, Items.BOW,
            Items.TRIPWIRE_HOOK, Items.CROSSBOW, /*Items.ARROW,*/ Items.TIPPED_ARROW
        );
        professionTradableItemsBuilder.putAll(VillagerProfession.LIBRARIAN,
            Items.PAPER, Items.ENCHANTED_BOOK, Items.BOOKSHELF,
            Items.BOOK, /*EnchantBookFactory,*/ Items.LANTERN,
            Items.INK_SAC, /*EnchantBookFactory,*/ Items.GLASS,
            Items.WRITABLE_BOOK, /*EnchantBookFactory,*/ Items.CLOCK, Items.COMPASS,
            Items.NAME_TAG
        );
        professionTradableItemsBuilder.putAll(VillagerProfession.CARTOGRAPHER,
            Items.PAPER, Items.MAP,
            Items.GLASS_PANE, //StructureTags.ON_OCEAN_EXPLORER_MAPS, "filled_map.monument",
                // MapDecorationTypes.MONUMENT,
            Items.COMPASS, //StructureTags.ON_WOODLAND_EXPLORER_MAPS, "filled_map.mansion", MapDecorationTypes.MANSION,
                // StructureTags.ON_TRIAL_CHAMBERS_MAPS, "filled_map.trial_chambers", MapDecorationTypes.TRIAL_CHAMBERS,
            Items.ITEM_FRAME,
                Items.WHITE_BANNER, Items.BLUE_BANNER, Items.LIGHT_BLUE_BANNER, Items.RED_BANNER,
                Items.PINK_BANNER, Items.GREEN_BANNER, Items.LIME_BANNER, Items.GRAY_BANNER,
                Items.BLACK_BANNER, Items.PURPLE_BANNER, Items.MAGENTA_BANNER, Items.CYAN_BANNER,
                Items.BROWN_BANNER, Items.YELLOW_BANNER, Items.ORANGE_BANNER, Items.LIGHT_GRAY_BANNER,
            Items.GLOBE_BANNER_PATTERN
        );
        professionTradableItemsBuilder.putAll(VillagerProfession.CLERIC,
            Items.ROTTEN_FLESH, Items.REDSTONE,
            Items.GOLD_INGOT, Items.LAPIS_LAZULI,
            Items.RABBIT_FOOT, Items.GLOWSTONE,
            Items.TURTLE_SCUTE, Items.GLASS_BOTTLE, Items.ENDER_PEARL,
            Items.NETHER_WART, Items.EXPERIENCE_BOTTLE
        );
        professionTradableItemsBuilder.putAll(VillagerProfession.ARMORER,
            Items.COAL, Items.IRON_LEGGINGS, Items.IRON_BOOTS, Items.IRON_HELMET, Items.IRON_CHESTPLATE,
            Items.IRON_INGOT, Items.BELL, Items.CHAINMAIL_BOOTS, Items.CHAINMAIL_LEGGINGS,
            Items.LAVA_BUCKET, Items.DIAMOND, Items.CHAINMAIL_HELMET, Items.CHAINMAIL_CHESTPLATE, Items.SHIELD,
            Items.DIAMOND_LEGGINGS, Items.DIAMOND_BOOTS,
            Items.DIAMOND_HELMET, Items.DIAMOND_CHESTPLATE
        );
        professionTradableItemsBuilder.putAll(VillagerProfession.WEAPONSMITH,
            Items.COAL, Items.IRON_AXE, Items.IRON_SWORD,
            Items.IRON_INGOT, Items.BELL,
            Items.FLINT,
            Items.DIAMOND, Items.DIAMOND_AXE,
            Items.DIAMOND_SWORD
        );
        professionTradableItemsBuilder.putAll(VillagerProfession.TOOLSMITH,
            Items.COAL, Items.STONE_AXE, Items.STONE_SHOVEL, Items.STONE_PICKAXE, Items.STONE_HOE,
            Items.IRON_INGOT, Items.BELL,
            Items.FLINT, Items.IRON_AXE, Items.IRON_SHOVEL, Items.IRON_PICKAXE, Items.DIAMOND_HOE,
            Items.DIAMOND, Items.DIAMOND_AXE, Items.DIAMOND_SHOVEL,
            Items.DIAMOND_PICKAXE
        );
        professionTradableItemsBuilder.putAll(VillagerProfession.BUTCHER,
            Items.CHICKEN, Items.PORKCHOP, Items.RABBIT, Items.RABBIT_STEW,
            Items.COAL, Items.COOKED_PORKCHOP, Items.COOKED_CHICKEN,
            Items.MUTTON, Items.BEEF,
            Items.DRIED_KELP_BLOCK, Items.SWEET_BERRIES
        );
        professionTradableItemsBuilder.putAll(VillagerProfession.LEATHERWORKER,
            Items.LEATHER, Items.LEATHER_LEGGINGS, Items.LEATHER_CHESTPLATE,
            Items.FLINT, Items.LEATHER_HELMET, Items.LEATHER_BOOTS,
            Items.RABBIT_HIDE, Items.LEATHER_CHESTPLATE,
            Items.TURTLE_SCUTE, Items.LEATHER_HORSE_ARMOR,
            Items.SADDLE, Items.LEATHER_HELMET
        );
        professionTradableItemsBuilder.putAll(VillagerProfession.MASON,
            Items.CLAY_BALL, Items.BRICK,
            Items.STONE, Items.CHISELED_STONE_BRICKS,
            Items.GRANITE, Items.ANDESITE, Items.DIORITE, Items.DRIPSTONE_BLOCK,
                Items.POLISHED_ANDESITE, Items.POLISHED_DIORITE, Items.POLISHED_GRANITE,
            Items.QUARTZ,
                Items.ORANGE_TERRACOTTA, Items.WHITE_TERRACOTTA, Items.BLUE_TERRACOTTA, Items.LIGHT_BLUE_TERRACOTTA,
                Items.GRAY_TERRACOTTA, Items.LIGHT_GRAY_TERRACOTTA, Items.BLACK_TERRACOTTA, Items.RED_TERRACOTTA,
                Items.PINK_TERRACOTTA, Items.MAGENTA_TERRACOTTA, Items.LIME_TERRACOTTA, Items.GREEN_TERRACOTTA,
                Items.CYAN_TERRACOTTA, Items.PURPLE_TERRACOTTA, Items.YELLOW_TERRACOTTA, Items.BROWN_TERRACOTTA,
                Items.ORANGE_GLAZED_TERRACOTTA, Items.WHITE_GLAZED_TERRACOTTA, Items.BLUE_GLAZED_TERRACOTTA,
                Items.LIGHT_BLUE_GLAZED_TERRACOTTA, Items.GRAY_GLAZED_TERRACOTTA, Items.LIGHT_GRAY_GLAZED_TERRACOTTA,
                Items.BLACK_GLAZED_TERRACOTTA, Items.RED_GLAZED_TERRACOTTA, Items.PINK_GLAZED_TERRACOTTA,
                Items.MAGENTA_GLAZED_TERRACOTTA, Items.LIME_GLAZED_TERRACOTTA, Items.GREEN_GLAZED_TERRACOTTA,
                Items.CYAN_GLAZED_TERRACOTTA, Items.PURPLE_GLAZED_TERRACOTTA, Items.YELLOW_GLAZED_TERRACOTTA,
                Items.BROWN_GLAZED_TERRACOTTA,
            Items.QUARTZ_PILLAR, Items.QUARTZ_BLOCK
        );

        PROFESSION_TRADABLE_ITEMS = professionTradableItemsBuilder.build();

        VILLAGER_TYPE_BOATS = ImmutableMap.<VillagerType, Item>builder()
            .put(VillagerType.PLAINS, Items.OAK_BOAT).put(VillagerType.TAIGA, Items.SPRUCE_BOAT)
            .put(VillagerType.SNOW, Items.SPRUCE_BOAT).put(VillagerType.DESERT, Items.JUNGLE_BOAT)
            .put(VillagerType.JUNGLE, Items.JUNGLE_BOAT).put(VillagerType.SAVANNA, Items.ACACIA_BOAT)
            .put(VillagerType.SWAMP, Items.DARK_OAK_BOAT)
            .build();

        LATER_ENCHANTABLE = ImmutableSet.of(Items.BOW, Items.CROSSBOW);
    }

    private VillagerEntityMixinConstants() { }

    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    public static final class DeprecatedDeserialization {
        private DeprecatedDeserialization() { }

        private static final ImmutableList<KeyMigration> KEY_MIGRATIONS = ImmutableList.of(
            new KeyMigration(Keys.TARGET_TRADES, "target_trade_stacks"),
            new KeyMigration(Keys.PENDING_REMOVAL, "pending_stacks")
        );

        public static PersistentData tryParseNbt(NbtCompound nbt, HolderLookup.Provider lookupProvider) {
            migrateNbt(nbt);
            final List<StackAndIndex> indexedTradeTargets = new LinkedList<>();
            {
                final List<ItemStack> targetTrades =
                    tryGetStacks(nbt, Keys.TARGET_TRADES, true, lookupProvider)
                        .orElseGet(List::of);

                final int size = targetTrades.size();
                for (int i = 0; i < size; i++) {
                    indexedTradeTargets.add(new StackAndIndex(targetTrades.get(i), i));
                }
            }

            final List<StackAndIndex> pendingSelection = new LinkedList<>();
            {
                tryGetStacks(nbt, Keys.PENDING_REMOVAL, false, lookupProvider).ifPresent(oldPendingRemoval -> {
                    int index = indexedTradeTargets.size();
                    for (final ItemStack oldStackPendingRemoval : oldPendingRemoval) {
                        pendingSelection.add(new StackAndIndex(oldStackPendingRemoval, index++));
                    }
                });
            }

            final List<ItemStack> pendingRejection =
                tryGetStacks(nbt, Keys.PENDING_REJECTION, true, lookupProvider)
                    .orElseGet(List::of);

            return new PersistentData(
                new TradeTargetManager(indexedTradeTargets, pendingSelection, pendingRejection),
                MAX_REJECTION_WAIT_TICKS,
                null
            );
        }

        private static void migrateNbt(NbtCompound nbt) {
            for (final var migration : KEY_MIGRATIONS) {
                for (final var deprecatedKey : migration.deprecatedKeys()) {
                    final var value = nbt.get(deprecatedKey);
                    if (value != null) {
                        nbt.remove(deprecatedKey);
                        nbt.put(migration.currentKey(), value);
                    }
                }
            }
        }

        private static Optional<List<ItemStack>> tryGetStacks(
            @NotNull NbtCompound nbt, String key, boolean required, HolderLookup.Provider lookupProvider
        ) {
            return tryGetNbtList(nbt, key, required)
                .map(stacksNbt -> {
                    final List<ItemStack> stacks = new LinkedList<>();
                    for (final var stackElement : stacksNbt) {
                        if (stackElement instanceof final NbtCompound stackCompound) {
                            stacks.add(ItemStack.fromNbt(lookupProvider, stackCompound));
                        } else {
                            LOGGER.error("\"{}\" contains unrecognized NBT: {}", key, stackElement);
                        }
                    }

                    return stacks;
                });
        }

        private static Optional<NbtList> tryGetNbtList(NbtCompound nbt, String key, boolean required) {
            final var element = nbt.get(key);
            if (element == null) {
                if (required) {
                    LOGGER.error("Expected NbtCompound to contain \"{}\", but found none!", key);
                }
                return Optional.empty();
            }

            if (!(element instanceof final NbtList list)) {
                LOGGER.error("Expected \"{}\" to be NbtList, but found {}", key, element.getClass());
                return Optional.empty();
            }

            return Optional.of(list);
        }

        private interface Keys {
            String TARGET_TRADES = "target_trades";
            String PENDING_REJECTION = "pending_rejection";
            String PENDING_REMOVAL = "pending_removal";
        }

        private record KeyMigration(String currentKey, ImmutableList<String> deprecatedKeys) {
            public KeyMigration(String currentKey, String... deprecatedKeys) {
                this(currentKey, ImmutableList.copyOf(deprecatedKeys));
            }
        }
    }

    public static class PersistentData {
        public static final Codec<UUID> UUID_CODEC = Codec.STRING.xmap(UUID::fromString, UUID::toString);

        public static final Codec<PersistentData> CODEC = RecordCodecBuilder.create(instance -> instance.group(
            TradeTargetManager.CODEC.optionalFieldOf(
                Keys.TRADE_TARGET_MANAGER, new TradeTargetManager()
            ).forGetter(data -> data.targetManager),
            Codec.INT.optionalFieldOf(
                Keys.REJECTION_WAIT_TICKS, 0
            ).forGetter(data -> data.rejectionWaitTicks),
            UUID_CODEC.optionalFieldOf(
                Keys.RECENT_INTERACTOR_UUID
            ).forGetter(data -> Optional.ofNullable(data.recentInteractorUuid))
        ).apply(instance, (manager, ticks, optUuid) -> new PersistentData(manager, ticks, optUuid.orElse(null))));

        @NotNull public TradeTargetManager targetManager;
        public int rejectionWaitTicks;
        @Nullable public UUID recentInteractorUuid;

        public PersistentData(
            @NotNull TradeTargetManager targetManager,
            Integer rejectionWaitTicks,
            @Nullable UUID recentInteractorUuid
        ) {
            this.targetManager = targetManager;
            this.rejectionWaitTicks = rejectionWaitTicks;
            this.recentInteractorUuid = recentInteractorUuid;
        }

        private interface Keys {
            String TRADE_TARGET_MANAGER = "trade_target_manager";
            String REJECTION_WAIT_TICKS = "rejection_wait_ticks";
            String RECENT_INTERACTOR_UUID = "recent_interactor_uuid";
        }
    }

    public record OfferAndIndex(TradeOffer offer, Integer index) { }
}
