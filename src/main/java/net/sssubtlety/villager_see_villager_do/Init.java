package net.sssubtlety.villager_see_villager_do;

import net.sssubtlety.villager_see_villager_do.config.ConfigManager;

import net.fabricmc.api.ModInitializer;

public class Init implements ModInitializer {
    @Override
    public void onInitialize() {
        ConfigManager.init();
    }
}
