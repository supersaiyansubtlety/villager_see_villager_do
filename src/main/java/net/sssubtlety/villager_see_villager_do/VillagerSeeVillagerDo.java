package net.sssubtlety.villager_see_villager_do;

import net.minecraft.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VillagerSeeVillagerDo {
	public static final String NAMESPACE = "villager_see_villager_do";
	public static final Text NAME = Text.translatable("text." + NAMESPACE + ".name");
	public static final Logger LOGGER = LoggerFactory.getLogger(NAMESPACE);
}
