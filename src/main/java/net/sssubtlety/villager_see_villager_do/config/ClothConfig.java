package net.sssubtlety.villager_see_villager_do.config;

import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.ConfigHolder;
import me.shedaniel.autoconfig.annotation.ConfigEntry;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import org.jetbrains.annotations.UnmodifiableView;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.ActionResult;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import static net.sssubtlety.villager_see_villager_do.VillagerSeeVillagerDo.NAMESPACE;

@me.shedaniel.autoconfig.annotation.Config(name = NAMESPACE)
public class ClothConfig implements ConfigData, Config {
    @ConfigEntry.Gui.Tooltip
    boolean pay_emerald_to_drop_items = DefaultConfig.INSTANCE.payEmeraldToDropItems();

    @ConfigEntry.Gui.Tooltip
    boolean return_selected_items = DefaultConfig.INSTANCE.returnSelectedItems();

    @ConfigEntry.Gui.Tooltip
    boolean villagers_drop_items_on_death = DefaultConfig.INSTANCE.villagersDropItemsOnDeath();

    @ConfigEntry.Gui.Tooltip
    boolean retain_item_name = DefaultConfig.INSTANCE.retainItemName();

    @ConfigEntry.Gui.Tooltip
    boolean retain_item_damage = DefaultConfig.INSTANCE.retainItemDamage();

    @ConfigEntry.Gui.Tooltip
    boolean retain_other_item_components = DefaultConfig.INSTANCE.retainOtherItemComponents();

    @ConfigEntry.Gui.Tooltip
    boolean limit_enchantments = DefaultConfig.INSTANCE.limitEnchantments();

    @ConfigEntry.Gui.Tooltip
    List<String> pickup_deny_list = DefaultConfig.INSTANCE.pickupDenyList();

    @ConfigEntry.Gui.Tooltip
    List<String> enchantment_deny_list = DefaultConfig.INSTANCE.enchantmentDenyList();

    @ConfigEntry.Gui.Tooltip
    List<String> potion_deny_list = DefaultConfig.INSTANCE.potionDenyList();

    @ConfigEntry.Gui.Tooltip
    List<String> effect_deny_list = DefaultConfig.INSTANCE.effectDenyList();

    static ClothConfig register(Consumer<ClothConfig> onConfigChange) {
        final ConfigHolder<ClothConfig> holder = AutoConfig.register(ClothConfig.class, GsonConfigSerializer::new);

        holder.registerLoadListener((loadedHolder, loadedConfig) -> {
            onConfigChange.accept(loadedConfig);

            return ActionResult.CONSUME;
        });

        holder.registerSaveListener((loadedHolder, loadedConfig) -> {
            onConfigChange.accept(loadedConfig);

            return ActionResult.CONSUME;
        });

        return holder.getConfig();
    }

    @Override
    public boolean payEmeraldToDropItems() {
        return this.pay_emerald_to_drop_items;
    }

    @Override
    public boolean returnSelectedItems() {
        return this.return_selected_items;
    }

    @Override
    public boolean villagersDropItemsOnDeath() {
        return this.villagers_drop_items_on_death;
    }

    @Override
    public boolean retainItemName() {
        return this.retain_item_name;
    }

    @Override
    public boolean retainItemDamage() {
        return this.retain_item_damage;
    }

    @Override
    public boolean retainOtherItemComponents() {
        return this.retain_other_item_components;
    }

    @Override
    public boolean limitEnchantments() {
        return this.limit_enchantments;
    }

    @Override
    @UnmodifiableView
    public List<String> pickupDenyList() {
        return this.pickup_deny_list;
    }

    @Override
    @UnmodifiableView
    public List<String> enchantmentDenyList() {
        return this.enchantment_deny_list;
    }

    @Override
    @UnmodifiableView
    public List<String> potionDenyList() {
        return this.potion_deny_list;
    }

    @Override
    @UnmodifiableView
    public List<String> effectDenyList() {
        return this.effect_deny_list;
    }

    @Override
    public Function<Screen, ? extends Screen> screenFactory() {
        return parent -> AutoConfig.getConfigScreen(ClothConfig.class, parent).get();
    }
}
