package net.sssubtlety.villager_see_villager_do.config;

import org.jetbrains.annotations.UnmodifiableView;

import net.minecraft.client.gui.screen.Screen;

import java.util.List;
import java.util.function.Function;

public interface Config {
    boolean payEmeraldToDropItems();

    boolean returnSelectedItems();

    boolean villagersDropItemsOnDeath();

    boolean retainItemName();

    boolean retainItemDamage();

    boolean retainOtherItemComponents();

    boolean limitEnchantments();

    @UnmodifiableView
    List<String> pickupDenyList();

    @UnmodifiableView
    List<String> enchantmentDenyList();

    @UnmodifiableView
    List<String> potionDenyList();

    @UnmodifiableView
    List<String> effectDenyList();

    Function<Screen, ? extends Screen> screenFactory();
}
