package net.sssubtlety.villager_see_villager_do.config;


import net.sssubtlety.villager_see_villager_do.VillagerSeeVillagerDo;
import net.sssubtlety.villager_see_villager_do.util.Util;

import com.google.common.collect.ImmutableList;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

import java.util.function.Function;

import static net.sssubtlety.villager_see_villager_do.VillagerSeeVillagerDo.NAMESPACE;

public final class DefaultConfig implements Config {
    public static final DefaultConfig INSTANCE = new DefaultConfig();

    private static final ImmutableList<String> PICKUP_DENY_LIST = ImmutableList.of();
    private static final ImmutableList<String> ENCHANTMENT_DENY_LIST = ImmutableList.of();
    private static final ImmutableList<String> POTION_DENY_LIST = ImmutableList.of();
    private static final ImmutableList<String> EFFECT_DENY_LIST = ImmutableList.of();

    private DefaultConfig() { }

    @Override
    public boolean payEmeraldToDropItems() {
        return true;
    }

    @Override
    public boolean returnSelectedItems() {
        return false;
    }

    @Override
    public boolean villagersDropItemsOnDeath() {
        return false;
    }

    @Override
    public boolean retainItemName() {
        return true;
    }

    @Override
    public boolean retainItemDamage() {
        return true;
    }

    @Override
    public boolean retainOtherItemComponents() {
        return true;
    }

    @Override
    public boolean limitEnchantments() {
        return true;
    }

    @Override
    public ImmutableList<String> pickupDenyList() {
        return PICKUP_DENY_LIST;
    }

    @Override
    public ImmutableList<String> enchantmentDenyList() {
        return ENCHANTMENT_DENY_LIST;
    }

    @Override
    public ImmutableList<String> potionDenyList() {
        return POTION_DENY_LIST;
    }

    @Override
    public ImmutableList<String> effectDenyList() {
        return EFFECT_DENY_LIST;
    }

    @Override
    public Function<Screen, ? extends Screen> screenFactory() {
        return NoConfigScreen::new;
    }

    private static final class NoConfigScreen extends Screen {
        private static final Text NO_CONFIG_SCREEN_TITLE =
            Text.translatable("text." + NAMESPACE + ".no_config_screen.title");
        private static final Text NO_CONFIG_SCREEN_MESSAGE =
            Text.translatable("text." + NAMESPACE + ".no_config_screen.message");

        // WHITE's color is non-null
        @SuppressWarnings("DataFlowIssue")
        private static final int TITLE_COLOR = Formatting.WHITE.getColorValue();
        // RED's color is non-null
        @SuppressWarnings("DataFlowIssue")
        private static final int MESSAGE_COLOR = Formatting.RED.getColorValue();

        private final Screen parent;

        private NoConfigScreen(Screen parent) {
            super(NO_CONFIG_SCREEN_TITLE);
            this.parent = parent;
        }

        @Override
        public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) {
            super.render(graphics, mouseX, mouseY, delta);
            final int windowHCenter = MinecraftClient.getInstance().getWindow().getScaledWidth() / 2;
            final int windowHeight = MinecraftClient.getInstance().getWindow().getScaledHeight();

            graphics.drawCenteredShadowedText(
                MinecraftClient.getInstance().textRenderer,
                Util.replace(NO_CONFIG_SCREEN_TITLE, "\\$\\{name\\}", VillagerSeeVillagerDo.NAME.getString()),
                windowHCenter, windowHeight / 10,
                TITLE_COLOR
            );

            graphics.drawCenteredShadowedText(
                MinecraftClient.getInstance().textRenderer,
                NO_CONFIG_SCREEN_MESSAGE,
                windowHCenter, windowHeight / 2,
                MESSAGE_COLOR
            );
        }

        @Override
        public void closeScreen() {
            //noinspection ConstantConditions; the method this overrides also dereferences this.client
            this.client.setScreen(this.parent);
        }
    }
}
