package net.sssubtlety.villager_see_villager_do.config;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.item.Item;
import net.minecraft.potion.Potion;
import net.minecraft.recipe.BrewingRecipeRegistry;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import static net.sssubtlety.villager_see_villager_do.VillagerSeeVillagerDo.LOGGER;
import static net.sssubtlety.villager_see_villager_do.util.Util.isModLoaded;

public final class ConfigManager {
    private ConfigManager() { }

    private static final Config CONFIG = isModLoaded("cloth-config", ">=6.0.42") ?
        ClothConfig.register(ConfigManager::update) : DefaultConfig.INSTANCE;

    private static final ImmutableMap<StatusEffect, Integer> SUSPICIOUS_STEW_EFFECT_DURATIONS = ImmutableMap.of(
        StatusEffects.NIGHT_VISION.getValue(), 100,
        StatusEffects.JUMP_BOOST.getValue(), 160,
        StatusEffects.WEAKNESS.getValue(), 140,
        StatusEffects.BLINDNESS.getValue(), 120,
        StatusEffects.POISON.getValue(), 280,
        StatusEffects.SATURATION.getValue(), 7
    );

    private static ImmutableSet<Item> deniedPickupItems;
    private static ImmutableSet<Identifier> deniedEnchantments;
    private static ImmutableSet<Potion> deniedPotions;
    private static ImmutableSet<StatusEffect> deniedEffects;

    static {
        update(CONFIG);
    }

    public static void init() { }

    public static boolean canPayEmeraldToDropItems() {
        return CONFIG.payEmeraldToDropItems();
    }

    public static boolean shouldReturnSelectedItems() {
        return CONFIG.returnSelectedItems();
    }

    public static boolean shouldVillagerDropItemsOnDeath() {
        return CONFIG.villagersDropItemsOnDeath();
    }

    public static boolean shouldRetainItemName() {
        return CONFIG.retainItemName();
    }

    public static boolean shouldRetainItemDamage() {
        return CONFIG.retainItemDamage();
    }

    public static boolean shouldRetainOtherItemComponents() {
        return CONFIG.retainOtherItemComponents();
    }

    public static boolean shouldLimitEnchantmentsEtc() {
        return CONFIG.limitEnchantments();
    }

    public static boolean isPickupDenied(Item item) {
        return deniedPickupItems.contains(item);
    }

    public static boolean isDenied(Enchantment enchantment, Registry<Enchantment> registry) {
        return deniedEnchantments.contains(registry.getId(enchantment));
    }

    public static boolean isAllowed(Potion potion, BrewingRecipeRegistry brewingRecipeRegistry) {
        return !deniedPotions.contains(potion) && (
            !ConfigManager.shouldLimitEnchantmentsEtc() || (
                !potion.getEffects().isEmpty() &&
                brewingRecipeRegistry.isBrewable(Registries.POTION.wrapAsHolder(potion))
            )
        );
    }

    public static boolean isAllowed(StatusEffect effect, int duration) {
        final Integer allowedDuration;
        return !deniedEffects.contains(effect) &&
            (!ConfigManager.shouldLimitEnchantmentsEtc() || (
                (allowedDuration = SUSPICIOUS_STEW_EFFECT_DURATIONS.get(effect)) != null &&
                allowedDuration == duration
            )
        );
    }

    public static Function<Screen, ? extends Screen> getScreenFactory() {
        return CONFIG.screenFactory();
    }

    private static void update(Config config) {
        deniedPickupItems = buildEntrySet(config.pickupDenyList(), Registries.ITEM);
        deniedEnchantments = idStreamOf(config.enchantmentDenyList()).collect(ImmutableSet.toImmutableSet());
        deniedPotions = buildEntrySet(config.potionDenyList(), Registries.POTION);
        deniedEffects = buildEntrySet(config.effectDenyList(), Registries.STATUS_EFFECT);
    }

    private static <T> ImmutableSet<T> buildEntrySet(List<String> ids, Registry<T> registry) {
        return idStreamOf(ids)
            .flatMap(id -> {
                final Optional<T> entry = registry.getOrEmpty(id);

                if (entry.isEmpty()) {
                    LOGGER.error("No \"{}\" with id: {}", registry.getKey().getValue(), id);
                }

                return entry.stream();
            })
            .collect(ImmutableSet.toImmutableSet());
    }

    private static Stream<Identifier> idStreamOf(List<String> ids) {
        return ids.stream().flatMap(string -> {
            final Identifier id = Identifier.tryParse(string);

            if (id == null) {
                LOGGER.error("Invalid identifier: {}", string);

                return Stream.empty();
            }

            return Stream.of(id);
        });
    }
}
