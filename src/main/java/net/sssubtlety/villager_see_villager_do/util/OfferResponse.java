package net.sssubtlety.villager_see_villager_do.util;

import net.minecraft.village.TradeOffer;

import java.util.function.Supplier;

public abstract sealed class OfferResponse {
    public static final class Reject extends OfferResponse {
        private Reject() { }

        public static final Reject INSTANCE = new Reject();
    }

    public static sealed class NonNegative extends OfferResponse {
        private NonNegative() { }

        public static final NonNegative INSTANCE = new NonNegative();
    }

    public static final class Accept extends NonNegative {
        public final Supplier<TradeOffer> offerFactory;

        public Accept(Supplier<TradeOffer> offerFactory) {
            this.offerFactory = offerFactory;
        }
    }
}
