package net.sssubtlety.villager_see_villager_do.util;

import net.minecraft.village.TradeOffer;

import java.util.function.Supplier;

public final class OfferResponses {
    private OfferResponses() { }

    public static final OfferResponse.Reject REJECT = OfferResponse.Reject.INSTANCE;

    public static final OfferResponse.NonNegative PASS = OfferResponse.NonNegative.INSTANCE;

    public static OfferResponse.Accept accept(Supplier<TradeOffer> offerFactory) {
        return new OfferResponse.Accept(offerFactory);
    }
}
