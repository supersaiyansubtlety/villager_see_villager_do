package net.sssubtlety.villager_see_villager_do.util;

import com.google.common.collect.ImmutableMap;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.VersionParsingException;
import net.fabricmc.loader.api.metadata.version.VersionPredicate;
import net.minecraft.component.DataComponent;
import net.minecraft.component.DataComponentType;
import net.minecraft.component.DataComponentTypes;
import net.minecraft.component.type.EnchantableComponent;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.predicate.DataComponentPredicate;
import net.minecraft.registry.Holder;
import net.minecraft.registry.HolderSet;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.registry.tag.EnchantmentTags;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.village.TradeOffer;
import net.minecraft.village.TradeOffers;
import net.minecraft.village.TradeableItem;

import net.sssubtlety.villager_see_villager_do.config.ConfigManager;
import net.sssubtlety.villager_see_villager_do.mixin.accessor.TradeOfferAccessor;

import it.unimi.dsi.fastutil.objects.Object2IntMap;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableInt;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.sssubtlety.villager_see_villager_do.VillagerSeeVillagerDo.LOGGER;

public final class Util {
    private Util() { }

    public static final int MAX_COST = 64;

    private static final float ENCHANTMENT_LEVEL_FACTOR = 0.15F;
    private static final int MAX_TOOL_ENCHANT_LEVEL = 19;

    private static final ImmutableMap<Identifier, DataComponentType<?>> INSUBSTANTIAL_COMPONENTS = Stream.of(
        DataComponentTypes.CUSTOM_NAME,
        DataComponentTypes.CUSTOM_MODEL_DATA,
        DataComponentTypes.LORE,
        DataComponentTypes.RARITY,
        DataComponentTypes.HIDE_ADDITIONAL_TOOLTIP,
        DataComponentTypes.HIDE_TOOLTIP,
        DataComponentTypes.ENCHANTMENT_GLINT_OVERRIDE,
        DataComponentTypes.DYED_COLOR
    ).collect(ImmutableMap.toImmutableMap(Registries.DATA_COMPONENT_TYPE::getId, Function.identity()));

    public static int intLog(int in, int base) {
        int log = 0;
        while (in > 0) {
            log++;
            in /= base;
        }

        return log;
    }

    // Creates a readable string representing the passed ItemStacks.
    public static String toString(Iterable<ItemStack> stacks) {
        final var stringBuilder = new StringBuilder();
        for (final ItemStack stack : stacks) {
            stringBuilder.append(stack.toString())
                .append("; components: [");

            stringBuilder.append(stack.getComponents().stream()
                .map(DataComponent::toString)
                .collect(Collectors.joining("; "))
            );

            stringBuilder.append("]")
                .append(System.lineSeparator());
        }

        return stringBuilder.toString();
    }

    // Creates a readable string representing the passed TradeOffer.
    public static String toString(TradeOffer newOffer) {
        return newOffer.getOriginalFirstBuyItem() + "; " + newOffer.getSellItem();
    }

    // Sets the emeralds count on one side of an offer.
    // Tries left side first, then right.
    // Logs an error if neither side has emeralds.
    public static void setEmeralds(TradeOffer offer, int cost) {
        final ItemStack curStack = offer.getOriginalFirstBuyItem();
        if (curStack.getItem() == Items.EMERALD) {
            curStack.setCount(cost);
        } else {
            offer.getSecondBuyItem()
                .filter(secondBuyItem -> secondBuyItem.item().getValue() == Items.EMERALD)
                .ifPresentOrElse(secondBuyItem -> ((TradeOfferAccessor)offer).villager_see_villager_do$setSecondBuyItem(
                    Optional.of(new TradeableItem(
                        secondBuyItem.item(), cost, secondBuyItem.components(),
                        secondBuyItem.itemStack().copyWithCount(cost)
                    ))
                ), () -> LOGGER.error("Expected emeralds in offer but found none: {}", toString(offer)));
        }
    }

    // Strips some nbt from the passed `ItemStack` according to configuration.
    public static void stripNbt(final ItemStack stack) {
        final var components = stack.getComponents();

        if (ConfigManager.shouldRetainOtherItemComponents()) {
            // keep original components and remove values according to configuration
            if (!ConfigManager.shouldRetainItemName()) {
                stack.remove(DataComponentTypes.CUSTOM_NAME);
            }
            if (!ConfigManager.shouldRetainItemDamage()) {
                stack.setDamage(0);
            }
        } else {
            // create a new components and only copy over enchantments and configured values
            final Set<Identifier> retainableComponents = stack.getItemComponents().stream()
                .map(DataComponent::type)
                .map(Registries.DATA_COMPONENT_TYPE::getId)
                .collect(Collectors.toCollection(HashSet::new));

            retainableComponents.add(Registries.DATA_COMPONENT_TYPE.getId(DataComponentTypes.STORED_ENCHANTMENTS));

            if (ConfigManager.shouldRetainItemName()) {
                retainableComponents.add(Registries.DATA_COMPONENT_TYPE.getId(DataComponentTypes.CUSTOM_NAME));
            }

            if (ConfigManager.shouldRetainItemDamage()) {
                retainableComponents.add(Registries.DATA_COMPONENT_TYPE.getId(DataComponentTypes.DAMAGE));
            }

            final var componentItr = components.iterator();
            while (componentItr.hasNext()) {
                if (!retainableComponents.contains(Registries.DATA_COMPONENT_TYPE.getId(componentItr.next().type()))) {
                    componentItr.remove();
                }
            }
        }
    }

    public static boolean hasSubstantialComponents(ItemStack stack) {
        if (EnchantmentHelper.hasEnchantments(stack)) {
            return true;
        }

        final var baseComponents = stack.getItemComponents();
        final var stackComponents = stack.getComponents();

        final MutableInt insubstantialComponentCount = new MutableInt(baseComponents.size());

        final Set<Identifier> insubstantialComponents = new HashSet<>();
        INSUBSTANTIAL_COMPONENTS.forEach((id, type) -> {
            if (stackComponents.get(type) != null) {
                insubstantialComponentCount.increment();
                insubstantialComponents.add(id);
            }
        });

        if (stackComponents.size() > insubstantialComponentCount.intValue()) {
            return true;
        }

        baseComponents.stream()
            .map(DataComponent::type)
            .map(Registries.DATA_COMPONENT_TYPE::getId)
            .forEach(insubstantialComponents::add);

        for (final var stackComponent : stackComponents) {
            if (!insubstantialComponents.contains(Registries.DATA_COMPONENT_TYPE.getId(stackComponent.type()))) {
                return true;
            }
        }

        return false;
    }

    // Calculates the cost of the passed `ItemStack` based on the passed `TradeOffers.Factory`.
    // The passed `ItemStack` is assumed to be enchanted.
    public static Optional<Integer> tryCalculateEnchantmentsCost(
        ItemStack enchantedStack, TradeOffers.Factory tradeFactory, Registry<Enchantment> registry
    ) {
        final Item enchantedItem = enchantedStack.getItem();
        final Set<Object2IntMap.Entry<Holder<Enchantment>>> enchantmentEntries =
            EnchantmentHelper.getEnchantments(enchantedStack).getEnchantmentEntries();

        final boolean limitEnchantments = ConfigManager.shouldLimitEnchantmentsEtc();
        if (enchantedItem == Items.ENCHANTED_BOOK) {
            if (limitEnchantments && enchantmentEntries.size() != 1) {
                return Optional.empty();
            } else {
                int totalCost = 0;
                for (final Object2IntMap.Entry<Holder<Enchantment>> entry : enchantmentEntries) {
                    final Holder<Enchantment> enchantmentHolder = entry.getKey();
                    final Enchantment enchantment = enchantmentHolder.getValue();
                    if (
                        ConfigManager.isDenied(enchantment, registry) ||
                        (limitEnchantments && !enchantmentHolder.isIn(EnchantmentTags.TRADEABLE))
                    ) {
                        return Optional.empty();
                    }

                    final int lvl = entry.getIntValue();
                    int cost = 2 + (new Random()).nextInt(5 + lvl * 10) + 3 * lvl;
                    if (enchantmentHolder.isIn(EnchantmentTags.DOUBLE_TRADE_PRICE)) {
                        cost *= 2;
                    }
                    totalCost += cost;
                }

                if (totalCost > MAX_COST) {
                    totalCost = MAX_COST;
                }

                return Optional.of(totalCost);
            }
        } else if (tradeFactory instanceof TradeOffers.SellEnchantedToolFactory toolFactory) {
            int enchantLevel = MAX_TOOL_ENCHANT_LEVEL;

            final EnchantableComponent enchantableComponent = enchantedStack.get(DataComponentTypes.ENCHANTABLE);
            final int enchantability = enchantableComponent == null ? 0 : enchantableComponent.value();
            if (enchantability <= 0) {
                return limitEnchantments ? Optional.empty() : Optional.of(MAX_COST);
            }

            enchantLevel += 1 + 2 * (enchantability / 4 + 1);
            enchantLevel = Math.max(
                1,
                Math.round((float) enchantLevel + (float) enchantLevel * ENCHANTMENT_LEVEL_FACTOR)
            );

            if (limitEnchantments && enchantmentEntries.size() > intLog(enchantLevel, 2)) {
                return Optional.empty();
            }

            int maxLevel = 0;
            for (final Object2IntMap.Entry<Holder<Enchantment>> entry : enchantmentEntries) {
                final Holder<Enchantment> enchantmentHolder = entry.getKey();
                final Enchantment enchantment = enchantmentHolder.getValue();

                if (ConfigManager.isDenied(enchantment, registry)) {
                    return Optional.empty();
                }

                final int level = entry.getIntValue();
                final var toolEntry = new EnchantmentLevelEntry(enchantmentHolder, level);

                if (limitEnchantments) {
                    final var impossible = new MutableBoolean(true);

                    final var toolEntryId = registry.getId(toolEntry.enchantment.getValue());
                    final int finalEnchantLevel = enchantLevel;
                    registry.getTag(EnchantmentTags.ON_TRADED_EQUIPMENT)
                        .map(HolderSet.ListBackedSet::stream)
                        .ifPresent(possibleEnchantments -> {
                            for (final var possibleEntry : EnchantmentHelper.getPossibleLevelEntries(
                                finalEnchantLevel, enchantedStack, possibleEnchantments
                            )) {
                                if (
                                    Objects.equals(toolEntryId, registry.getId(possibleEntry.enchantment.getValue())) &&
                                    toolEntry.level <= possibleEntry.level
                                ) {
                                    impossible.setFalse();
                                    break;
                                }
                            }
                        });

                    if (impossible.isTrue()) {
                        return Optional.empty();
                    }
                }

                maxLevel = Math.max(maxLevel, level);
            }

            return (limitEnchantments && maxLevel + 5 > MAX_TOOL_ENCHANT_LEVEL) ?
                Optional.empty() :
                Optional.of(Math.min(toolFactory.basePrice + maxLevel, MAX_COST));
        } else {
            return limitEnchantments ?
                Optional.empty() :
                // can't compute a cost, just do max cost
                Optional.of(MAX_COST);
        }
    }

    @SuppressWarnings("ConstantConditions")
    public static int getRgb(Text text) {
        final var color = text.getStyle().getColor();
        if (color == null) {
            return Formatting.WHITE.getColorValue();
        } else {
            return color.getRgb();
        }
    }

    public static Text replace(Text text, String regex, String replacement) {
        String string = text.getString();
        string = string.replaceAll(regex, replacement);
        return Text.literal(string).setStyle(text.getStyle());
    }

    public static boolean isModLoaded(String id, String versionPredicate) {
        final var modContainer = FabricLoader.getInstance().getModContainer(id);
        if (modContainer.isPresent()) {
            try {
                return VersionPredicate.parse(versionPredicate).test(modContainer.get().getMetadata().getVersion());
            } catch (VersionParsingException e) {
                LOGGER.error("Error parsing version", e);
            }
        }

        return false;
    }

    public static DataComponentPredicate createComponentPredicate(ItemStack stack) {
        final var predicate = DataComponentPredicate.builder();
        for (final var component : stack.getComponents()) {
            final var type = component.type();
            if (!INSUBSTANTIAL_COMPONENTS.containsKey(Registries.DATA_COMPONENT_TYPE.getId(type))) {
                put(predicate, component);
            }
        }

        return predicate.build();
    }

    private static <T> void put(DataComponentPredicate.Builder predicate, DataComponent<T> component) {
        predicate.add(component.type(), component.value());
    }
}
