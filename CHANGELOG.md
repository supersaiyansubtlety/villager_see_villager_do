- 1.4.1 (21 Dec. 2024):
  - Marked as compatible with 1.21.4
  - Minor internal changes
- 1.4.0 (3 Dec 2024):
  - Updated for 1.21.2-1.21.3
  - Minor internal changes
- 1.3.1 (4 Apr. 2024): Marked as compatible with 1.21.1
- 1.3.0 (14 Jul. 2024):
  - Updated for 1.21!
  - Enchanted item limitation and cost checks now account for vanilla's `"minecraft:tradeable"`,
  `"minecraft:double_trade_price"`, and `"minecraft:on_traded_quipment"` enchantment tags; improves mod compatibility
  - Replaced bundled [CrowdinTranslate](<https://github.com/gbl/CrowdinTranslate>) with optional
  [SSS Translate](<https://modrinth.com/mod/sss-translate>) dependency;
  install [SSS Translate](<https://modrinth.com/mod/sss-translate>) for automatic translation updates
  - Minor internal changes
- 1.2.1 (19 May 2024): Updated for 1.20.5 and 1.20.6
- 1.2.0 (19 May 2024):
  - Added new config options:
    - Enchantment deny list: Villagers won't select gathered items for trade if they have enchantments in this list.
    - Potion deny list: Villagers won't select gathered items for trade if they contain potions in this list
    (affects fletchers' tipped arrows).
    - Effect deny list: Villagers won't select gathered items for trade if they have effects in this list
    (affects farmers' suspicious stews).
  - Boats will no longer be picked up by fishermen if they're not the correct type
  - Moderate internal changes and improvements
- 1.1.13 (25 Jan. 2024):
  - Updated for 1.20.2-1.20.4
  - Minor internal changes
  - No issues were reported regarding 1.1.12-b2, so this is a full release!
- 1.1.12-b2 (5 Aug. 2023):
  - Added new config "Villagers return selected items" (default false)  
  If true, when a villager selects a trade based on an item they were thrown, 
  they'll throw that item back to the player.
  - Villagers will now try to select trades for thrown items in the order they were received.
  - Villagers will now throw back rejected items sooner.
  - Villagers will no longer gather items that they threw, even if they would do so in vanilla.
  - Villagers now try *even harder* to throw items to the most recent player (or other entity) they've interacted with.
  - Reworked how persistent data is saved and retrieved
  - Fixed villagers not charging an emerald to return gathered items
  - Fixed rejecting tipped arrows with potions with modifiers
  - Fixed setting suspicious stew trades
- 1.1.12-b1 (20 Jun. 2023): Marked as compatible with 1.20.1
- 1.1.11-b1 (20 Jun. 2023): Updated for 1.20!
- 1.1.10-b1 (20 Jun. 2023):
  - Added new config 'Limit enchantments' (default true), that when false,
    will allow enchanted items to have any enchantments on them, not just those possible in vanilla.
    Closes [#8](https://gitlab.com/supersaiyansubtlety/villager_see_villager_do/-/issues/8).
  - Villagers will now close any open trading screens when they pick up an item and re-roll their trades
  - Villagers now try harder to throw items to a player rather than it dropping on the ground
  - Refined some error logging
  - Fixed villagers not returning rejected items as they should have been
  - Fixed incorrectly logging error "Found rejected stacks that aren't in inventory:..."
  - Fixed several trade selection issues that could prevent selection of valid items after an invalid item was rejected
  - Reworked some internals to be easier to maintain and less prone to errors
  - This is a **BETA** version: there were a significant number of internal changes so there's a higher-than-normal
  chance that new issues were introduced
- 1.1.9 (23 Mar. 2023): Updated for 1.19.4
- 1.1.8 (7 Aug. 2022): Marked as compatible with 1.19.2
- 1.1.7 (4 Aug. 2022): Allow controlling of mason villagers' dripstone trade
- 1.1.6-3 (4 Aug. 2022): Remove 1.18.2 support from 1.19 version
- 1.1.6-2 (4 Aug. 2022): Created 1.18.2 version
- 1.1.6-1 (4 Aug. 2022): Fix crash on 1.18/1.18.1 due to wrong crowdin translate version
- 1.1.6 (3 Aug. 2022):
  - Fixed bug that caused certain items that were allowed to be damaged but not allowed to be enchanted to be
  improperly rejected.
  - Changed some error logging to warning logging.
- 1.1.5 (29 Jul. 2022): Marked as compatible with 1.19.1
- 1.1.4 (22 Jun. 2022):
  - Updated for 1.18.2 AND 1.19!
  - Cloth config is now optional.
- 1.1.3 (13 Dec. 2021): Marked as compatible with 1.18.1
- 1.1.2 (3 Dec. 2021):
  - Updated for 1.18!
  - You no longer need to restart the game for changes pickupBlackList to take effect.
- 1.1.1 (24 Aug. 2021):
  - Added pickup blacklist. Villagers won't pickup items on the blacklist (they'll still pickup items like certain
  crops that they'd pickup without the mod installed).
  Closes [#3](https://gitlab.com/supersaiyansubtlety/villager_see_villager_do/-/issues/3)
  - Villagers will no longer jump out of bed to pickup items.
  Closes [#5](https://gitlab.com/supersaiyansubtlety/villager_see_villager_do/-/issues/5)
  - Fisherman villagers will once again only trade boats appropriate for their biome type.
  Closes [#4](https://gitlab.com/supersaiyansubtlety/villager_see_villager_do/-/issues/4).
  - Fixed incorrect stack counts and items being assigned to the wrong sides of trades.
- 1.1-b (5 Aug. 2021):
  - This is a **beta** version, there's a higher-than-usual chance that you will encounter issues.  
Please report any issues on the [issue tracker](https://gitlab.com/supersaiyansubtlety/villager_see_villager_do/-/issues).
  - Fixed controlling fletchers' tipped arrow trades
  - Rewrote the code that selects which offers are gained at each level.  
  The code is now less awful.  
  The only difference you should notice in-game is that trades you've controlled will always be the first gained when a
  villager levels up.
- 1.0.5 (10 Jul. 2021): Marked as compatible with 1.17.1.
- 1.0.4 (18 Jun. 2021): Fixed an issue that could prevent you from controlling multiple trades when those trades would
be gained at the same level (including level 0).
- 1.0.3 (13 Jun. 2021):
  - Updated for 1.17.
  - Cloth Config and Auto Config (updated) are no longer bundled, you must download them separately.
- 1.0.2 (22 Feb. 2021): Added new options:
  - Villagers drop items on death
  - Retain item names
  - Retain item damage
  - Retain other item NBT
- 1.0.1 (7 Feb. 2021):
  - Changed how enchanted tools and armor are checked to be less restrictive.  
  It should match vanilla more closely now, please let me know if any trades are/aren't being rejected when they
  shouldn't/should be.  
  There's also now an option to disable crowdin translation updates.
- 1.0 (15 Jan. 2021):
  - First full release version!
  - Marked as compatible with 1.16.5.
  - Translations are now handled with CrowdinTranslate.
- 0.1.1-beta (5 Dec. 2020): Fixed configs initializing client-side. Works properly on servers now. Also now allows new
Fabric API versions.
- 0.1-beta (2 Dec. 2020): Initial public beta version.
- 0.0.1 (23 Nov. 2020): Initial version. 